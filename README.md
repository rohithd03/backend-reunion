# To Install Dependencies ``` npm install ```

# To start the server ``` npm start ```

# Add a .env file which should contain:
```
PORT - localhost port number
DB_URL - MongoDB url
PVT_KEY - secret key to generate token
```

# APIs
### POST ``` /api/authenticate ```
```
Request body should contain a JSON object of:
{
    "email" : your_username,
    "password" : your_password
}

It returns a token, which is used as Bearer token.
```

### POST ``` /api/follow/:id ```
```
The authenticated user will follow user with user id - id.
```

### POST ``` /api/unfollow/:id ```
```
The Authenticated user will unfollow a user with user id - id.
```

### GET ``` /api/user ```
```
Return details of authenticated user.
```

### POST ``` /api/posts/ ```
```
Creates a new post by the authenticated user.
Request body should contain a JSON object of:
{
    "title" : post_title,
    "description" : post_description
}
Returns post details.
```

### DELETE ``` /api/posts/:id ```
```
Deletes post with id - id, created by the authenticated user.
```

### POST ``` /api/like/:id ```
```
Adds like to the post with id - id, by the authenticated user.
```

### POST ``` /api/unlike/:id ```
```
Removes like to the post with id - id, by the authenticated user.
```

### POST ``` /api/comment/:id ```
```
Adds comment to the post with id - id, by the authenticated user.
Request body should contain a JSON object of:
{
    "comment" : your_comment
}
Returns comment details.
```

### GET ``` /api/posts/:id ```
```
Return details of the post with id - id.
```

### GET ``` /api/all_posts ```
```
Returns all posts created by the authenticated user.
```

# All the above APIs have been deployed on heroku

Find the link [here](https://be-reunion.herokuapp.com/)

### To play with the APIs use the following user details:
```
Alice: {
    email: alice@example.com,
    password: 1234
},
Bob: {
    email: bob@example.com,
    password: 123
}
```
