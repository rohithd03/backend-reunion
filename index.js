require('dotenv').config();
const express = require('express')
const mongoose = require('mongoose')

const app = require('./src/api/index.js');

const PORT = process.env.PORT || 3000;

const uri = process.env.DB_URL || "mongodb://localhost:27017/socialmedia";

async function connectDB(){
    mongoose.connect(uri, {useNewUrlParser: true})
}

app.listen(PORT, async function(){
    await connectDB();
    console.log(`Server started at port ${PORT}`);
});

process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
});

process.on('uncaughtException', function (err) {
    console.error(err);
});