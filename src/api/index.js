require('dotenv').config();
const express = require("express");
const path = require('path');

const app = express();
const userRoutes = require("./v1/routes/userRoutes.js");

app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));


app.use("/api", userRoutes)

app.get("/ping", (req, res) => {
    res.send("Hello world!");
})

app.use((req, res, next)=>{
	const err = new Error("not found")
	err.status = 404
	next(err)
})

app.use((error, req, res, next)=>{
	return res.status(error.status || 500).json({
		error: {
			message: error.message
		}
	})
})



module.exports = app;