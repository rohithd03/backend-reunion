const User = require("../models/user.js");
const Post = require("../models/post.js");
const Comment = require("../models/comment.js");

const schemas = [User, Post, Comment];

module.exports.findOneDocument = async (i, query) => {
    try {
        const result = await schemas[i].findOne(query)
        return result;
    } catch (error) {
        return error;
    }
}

module.exports.deleteone = async (i, value) => {
    try {
        const result = await schemas[i].deleteOne({"_id": value});
        return result;
    } catch (error) {
        return error;
    }
}