const services = require('../services/userServices');
const Post = require('../models/post');

module.exports.createpost = async (req, res, next) => {
    try {
        const me = await services.findOneDocument(0, {username: req.username});
        if(req.body.title && req.body.description){
            let post = new Post({
                author: me.id,
                title: req.body.title,
                description: req.body.description
            })
    
            await post.save();
    
            return res.status(200).send({
                "Post-ID": post.id,
                "Title": post.title,
                "Description": post.description,
                "Created Time": post.created_at
            })
        }
        var err = new Error("Required Title and Description!");
        err.status = 400;
        throw err;
    } catch (error) {
        next(error);
    }
}

module.exports.deletepost = async (req, res, next) => {
    try {
        const me = await services.findOneDocument(0, {username: req.username});
        const mypost = await services.findOneDocument(1, {"_id": req.params.id});
        
        if(!mypost){
            return res.status(400).send({"message": "Post does not exist."});
        }
        
        if(mypost.author.equals(me.id)){
            await services.deleteone(1, mypost.id);
            return res.status(200).send({"Success": true, "message": "Post has been deleted successfully."})
        }
        else {
            return res.status(403).send({"message": "You're not Authorised to delete this post."})
        }

    } catch (error) {
        next(error)
    }
}

module.exports.allposts = async (req, res, next) => {
    try {
        const me = await services.findOneDocument(0, {username: req.username});
        const allposts = await Post.find({author: me.id}).sort({"created_at": 1}).populate({path: "comments", select: "comment"});
        const posts = allposts.map((p) => {
            return {
                "ID": p.id,
                "Title": p.title,
                "Description": p.description,
                "Created_at": p.created_at,
                "Comments": p.comments,
                "likes": p.likes.length,
                "Likes": p.likes
            }
        })
        console.log(posts)
        return res.status(200).send(posts)
    } catch (error) {
        next(error)
    }
}

module.exports.getpost = async (req, res, next) => {
    try {
        const me = await services.findOneDocument(0, {username: req.username});
        const p = await Post.findOne({"_id": req.params.id}).populate({path: "comments", select: "comment"});
        
        if(!p){
            return res.status(400).send({"message": "Post does not exist."});
        }
        
        const post =  {
                "ID": p.id,
                "Title": p.title,
                "Description": p.description,
                "Created_at": p.created_at,
                "Comments": p.comments,
                "likes": p.likes.length
            }
        
        return res.status(200).send(post)
    } catch (error) {
        next(error)
    }
}

module.exports.like = async (req, res, next) => {
    try {
        const me = await services.findOneDocument(0, {username: req.username});
        const post = await services.findOneDocument(1, {"_id": req.params.id});

        if(!post){
            return res.status(400).send({"message": "Post does not exist."});
        }

        if(!post.likes.includes(me.id)){
            post.likes.push(me.id);
            await post.save()

            return res.status(200).send({"Success": true, "message": "You've successfully liked the post."})
        }
        else {
            return res.status(200).send({"message": "You've already liked the post."})
        }
    } catch (error) {
        next(error)
    }
}

module.exports.unlike = async (req, res, next) => {
    try {
        const me = await services.findOneDocument(0, {username: req.username});
        const post = await services.findOneDocument(1, {"_id": req.params.id});

        if(!post){
            return res.status(400).send({"message": "Post does not exist."});
        }

        if(post.likes.includes(me.id)){
            post.likes.pull(me.id);
            await post.save()

            return res.status(200).send({"Success": true, "message": "You've successfully unliked the post."})
        }
        else {
            return res.status(200).send({"message": "You've not liked the post yet."})
        }
    } catch (error) {
        next(error)
    }
}