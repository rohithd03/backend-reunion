const services = require('../services/userServices')
const Comment = require('../models/comment')

module.exports.addcomment = async (req, res, next) => {
    try {
        const me = await services.findOneDocument(0, {username: req.username});
        const post = await services.findOneDocument(1, {"_id": req.params.id});

        if(!post){
            return res.status(400).send({"message": "Post does not exist."});
        }
        let comment = new Comment({
            user: me.id,
            post: req.params.id,
            comment: req.body.comment
        });

        await comment.save();

        post.comments.push(comment.id);
        await post.save()

        return res.status(200).send({
            "message": "Comment added successfully.",
            "Comment-ID": comment.id
        });
    } catch (error) {
        next(error)
    }
}