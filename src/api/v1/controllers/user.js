const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const services = require('../services/userServices')

module.exports.auth = async (req, res, next) => {
    try {
        if(req.body.email && req.body.password){
            const result = await services.findOneDocument(0, {email: req.body.email});
            if(!result){
                var err = new Error("Incorrect Credentials.");
                err.status = 400;
                throw err;
            }
            if(req.body.password === result.hash){
                const token = jwt.sign({username: result.username, email: result.email}, process.env.PVT_KEY, { expiresIn: '1d' });
                return res.status(200).send({"message": "Authentication successful, use token as Bearer token.","token": token})
            }
        }
        var err = new Error("Required Email and password!");
        err.status = 400;
        throw err;
    } catch (error) {
        next(error);
    }
}

module.exports.follow = async (req, res, next) => {
    try {
        const me = await services.findOneDocument(0, {username: req.username});

        const friend = await services.findOneDocument(0, {"_id": req.params.id});
        if(!friend){
            return res.status(400).send({"message": "User does not exist."});
        }

        if(me.following.includes(req.params.id)){
            return res.status(200).send({"message": "You're already following the user."});
        }
        else {
            me.following.push(req.params.id);
            await me.save();

            friend.followers.push(me.id);
            await friend.save();

            return res.status(200).send({"success": true, "message": "You are following the user now."})
        }
    
    } catch (error) {
        next(error);
    }
}

module.exports.unFollow = async (req, res, next) => {
    try {
        const me = await services.findOneDocument(0, {username: req.username});

        const friend = await services.findOneDocument(0, {"_id": req.params.id});
        if(!friend){
            return res.status(400).send({"message": "User does not exist."});
        }

        if(me.following.includes(req.params.id)){
            me.following.pull(req.params.id);
            await me.save();

            friend.followers.pull(me.id);
            await friend.save();

            return res.status(200).send({"success": true, "message": "You are successfully unfollowed the user now."})
        }
        else {
            return res.status(200).send({"message": "You're not following the user to unfollow."});
        }
    
    } catch (error) {
        next(error);
    }
}

module.exports.user = async (req, res, next) => {
    try {
        const me = await services.findOneDocument(0, {username: req.username});

        return res.status(200).send({
            "Username": me.username,
            "Email": me.email,
            "Number of Followings": me.following.length,
            "Following": me.following,
            "Number of Followers": me.followers.length,
            "Followers": me.followers
        });
    } catch (error) {
        next(error);
    }
}