const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
    username: {
        type: String,
        requires: true,
        unique: true
    },
    hash: {
        type: String,
        required: true
    },
    email: {
        type: String,
        requires: true,
        unique: true
    },
    followers: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    following: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }]
}, {timestamps: {
    createdAt: "created_at"
}})

module.exports = mongoose.model("User", userSchema)