const express = require('express');
const app = express.Router();
const userController = require('../controllers/user');
const postController = require('../controllers/post');
const commentController = require('../controllers/comment')
const verifyToken = require('../middlewares/index');

app.post("/authenticate", userController.auth);

app.get("/user", verifyToken, userController.user);

app.post("/follow/:id", verifyToken, userController.follow);
app.post("/unfollow/:id", verifyToken, userController.unFollow);


app.post("/posts", verifyToken, postController.createpost);
app.delete("/posts/:id", verifyToken, postController.deletepost);
app.get("/posts/:id", postController.getpost)
app.get("/all_posts", verifyToken, postController.allposts);

app.post("/like/:id", verifyToken, postController.like);
app.post("/unlike/:id", verifyToken, postController.unlike);

app.post("/comment/:id", verifyToken, commentController.addcomment);
module.exports = app;