const jwt = require('jsonwebtoken');

async function verifyToken(req, res, next){
    try {
        const token = req.headers.authorization.split(" ")[1];
        const details = jwt.verify(token, process.env.PVT_KEY);
        req.username = details.username;
        req.email = details.email;
        next();
    } catch (error) {
        var err = new Error("Invalid Token.");
        err.status = 403;
        next(err);
    }
}

module.exports = verifyToken;